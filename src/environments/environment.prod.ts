export const environment = {
  production: true,
  firebase: {
    apiKey: "AIzaSyCaK39rgAozRNOtxwobd94wZsJ9W_u3DJo",
    authDomain: "app-arquitectura-43da8.firebaseapp.com",
    databaseURL: "https://app-arquitectura-43da8.firebaseio.com",
    projectId: "app-arquitectura-43da8",
    storageBucket: "app-arquitectura-43da8.appspot.com",
    messagingSenderId: "1061613567644",
    appId: "1:1061613567644:web:4578747884eb7076fb7759",
    measurementId: "G-KPLCKCPXDP"
  }
};
