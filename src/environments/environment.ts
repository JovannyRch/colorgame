// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: true,
  firebase: {
    apiKey: "AIzaSyCaK39rgAozRNOtxwobd94wZsJ9W_u3DJo",
    authDomain: "app-arquitectura-43da8.firebaseapp.com",
    databaseURL: "https://app-arquitectura-43da8.firebaseio.com",
    projectId: "app-arquitectura-43da8",
    storageBucket: "app-arquitectura-43da8.appspot.com",
    messagingSenderId: "1061613567644",
    appId: "1:1061613567644:web:4578747884eb7076fb7759",
    measurementId: "G-KPLCKCPXDP"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
