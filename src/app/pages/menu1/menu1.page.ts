import { Component, OnInit, OnDestroy } from "@angular/core";
import { Storage } from "@ionic/storage";
import { ApiService } from "../../services/api.service";
import { Router } from "@angular/router";

@Component({
  selector: "app-menu1",
  templateUrl: "./menu1.page.html",
  styleUrls: ["./menu1.page.scss"]
})
export class Menu1Page implements OnInit {
  constructor(
    private storage: Storage,
    public api: ApiService,
    public router: Router
  ) {}

  usuarioStorage: any = {
    username: "",
    record: null
  };

  modoSeleccionado: any;

  usuarioDB: any = {};
  cargando: boolean = true;
  intervalo: any = {};
  segundos: number = 2;
  colorUsuario: any = {
    r: 0,
    g: 0,
    b: 0
  };

  puntuaciones: any = [];

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    //TODO: Quitar intervalo cuando se cierre la pagina
    clearInterval(this.intervalo);
  }
  ngOnInit() {
    this.cargando = true;
    this.colorUsuario = this.randomColor();

    // Configuración del juego
    this.storage.get("usuario").then(val => {
      if (val.username) {
        this.usuarioStorage.username = val.username;
        this.getDatosUsuario();
      } else {
        console.log("Error al obtener usuario Storage");
      }
    });

    this.intervalo = setInterval(() => {
      this.controlTiempo();
    }, 1000);
  }

  getDatosUsuario() {
    let users = this.api.getUsuario(this.usuarioStorage.username);

    users.forEach(user => {
      if (user.length > 0) {
        let e = user[0];
        this.usuarioDB = {
          id: e.payload.doc.id,
          username: e.payload.doc.data()["username"],
          record: e.payload.doc.data()["record"]
        };
        this.storage.set("usuario", this.usuarioDB);
      } else {
        console.log("error");
      }
    });
    this.cargando = false;
  }

  seleccionarModo(modo: number) {
    //Ir directemente al modo de juego
    if (modo == 1) this.router.navigate(["/rush"]);
    if (modo == 2) this.router.navigate(["/multijugador"]);
    if (modo == 3) this.router.navigate(["/clasico"]);
    if (modo == 4) this.router.navigate(["/trueorfalse"]);
  }

  controlTiempo() {
    console.log(this.segundos);

    this.segundos -= 1;
    if (this.segundos == 0) {
      this.segundos = 2;
      this.colorUsuario = this.randomColor();
    }
  }

  getRGB(color) {
    return `rgb(${color.r}, ${color.g}, ${color.b})`;
  }

  randomColor() {
    let r = {
      r: 0,
      g: 0,
      b: 0
    };
    r.r = Math.floor(Math.random() * 255);
    r.g = Math.floor(Math.random() * 255);
    r.b = Math.floor(Math.random() * 255);
    return r;
  }
}
