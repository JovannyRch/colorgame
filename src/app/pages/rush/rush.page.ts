import { Component, OnInit, OnDestroy } from "@angular/core";

//Vibración
import { Vibration } from "@ionic-native/vibration/ngx";

import { ActivatedRoute } from "@angular/router";
import { Storage } from "@ionic/storage";

import { ApiService } from "../../services/api.service";

@Component({
  selector: "app-rush",
  templateUrl: "./rush.page.html",
  styleUrls: ["./rush.page.scss"]
})
export class RushPage implements OnInit, OnDestroy {
  constructor(
    private activeRoute: ActivatedRoute,
    private storage: Storage,
    public vibration: Vibration,
    public api: ApiService
  ) {}

  ngOnDestroy() {
    console.log("Saliendo de Rush");

    clearInterval(this.intervalo);
  }

  pregunta: string;
  correcta: string;
  respuestas: any[] = [];

  puntuacion: number = 0;
  vidas: number = 3;

  //Parametros del juego
  segundos: number = 180;
  tiempoString: string = "";
  nivel: number = 0;

  tiempoInicial: number = 63;
  tipoTiempo: number = 0;
  intervalo: any;

  rachaActual: number = 0;
  rachaMax: number = 0;
  promXproblema: number = 0;
  tiempoHecho = 0;

  datosJuego: any = {
    record: 0,
    intentos: 0,
    puntosAcum: 0,
    rachaMax: 0,
    tiempoAcum: 0,
    ejerciciosAcum: 0,
    fallos: 0,
    aciertos: 0
  };

  // Banderas
  //TODO: cambiar a false
  jugando: boolean = false;
  finIntento: boolean = false;
  nuevoRecord: boolean = false;
  conteo: boolean = false;
  ultimosSegundos: boolean = false;
  finTiempo: boolean = false;

  //Audios
  audioCorrect = new Audio("assets/audio/correcto.mp3");
  audioIncorrect = new Audio("assets/audio/incorrecto.mp3");
  audioConteo = new Audio("assets/audio/conteo.mp3");
  audio30Secods = new Audio("assets/audio/30seconds.webm");
  audioFinNormal = new Audio("assets/audio/resultado_normal.mp3");
  audioFinBueno = new Audio("assets/audio/resultado_bueno.mp3");
  audioFinMalo = new Audio("assets/audio/resultado_malo.mp3");

  conf: any = {
    dificultad: 1,
    tiempo: 1,
    temaOscuro: false,
    dosJugadores: false,
    idioma: null
  };

  coloresReales: any = {
    r: 0,
    g: 0,
    b: 0
  };

  usuarioStorage: any = {
    username: "",
    record: null
  };

  cargando: boolean = true;

  usuarioDB: any = {};

  ngOnInit() {
    // Configuración del juego
    this.storage.get("conf").then(val => {
      if (val) this.conf = val;
      else this.storage.set("conf", this.conf);
    });

    // Datos del usuario
    this.storage.get("usuario").then(val => {
      if (val.username) {
        this.usuarioStorage.username = val.username;
        this.getDatosUsuario();
      } else {
        console.log("Error al obtener usuario Storage");
      }
    });

    //TODO: Comentar
    //this.jugando = false;
    //this.randomColor(1);
    //this.generarRespuestas();
    //this.jugando = true;
    //this.finIntento = true;
    //this.tiempoInicial = 13;
  }

  randomColor() {
    this.coloresReales.r = Math.floor(Math.random() * 255);
    this.coloresReales.g = Math.floor(Math.random() * 255);
    this.coloresReales.b = Math.floor(Math.random() * 255);
  }

  getRango() {
    if (this.nivel < 5) {
      return {
        min: 20,
        max: 60
      };
    }

    if (this.nivel < 11) {
      return {
        min: 18,
        max: 45
      };
    }

    if (this.nivel < 20) {
      return {
        min: 17,
        max: 37
      };
    }

    if (this.nivel < 25) {
      return {
        min: 12,
        max: 33
      };
    }
    if (this.nivel < 30) {
      return {
        min: 10,
        max: 25
      };
    }

    if (this.nivel < 35) {
      return {
        min: 7,
        max: 13
      };
    }

    if (this.nivel < 40) {
      return {
        min: 4,
        max: 11
      };
    }

    if (this.nivel < 45) {
      return {
        min: 3,
        max: 7
      };
    }
    return {
      min: 1,
      max: 3
    };
  }

  getDatosUsuario() {
    let users = this.api.getUsuario(this.usuarioStorage.username);

    users.forEach(user => {
      if (user.length > 0) {
        let e = user[0];
        this.usuarioDB = {
          id: e.payload.doc.id,
          username: e.payload.doc.data()["username"],
          recordR: e.payload.doc.data()["recordR"]
            ? e.payload.doc.data()["recordR"]
            : 0
        };
        console.log(this.usuarioDB);
        this.datosJuego.record = this.usuarioDB.recordR;
      } else {
        console.log("error");
      }
    });
    this.cargando = false;
  }

  getRGB(color) {
    return `rgb(${color.r}, ${color.g}, ${color.b})`;
  }

  generarRespuestas() {
    // Entre mas fácil sea el nivel, mas lejano deben estar los valores de las posibles respuestas
    this.respuestas = [];
    //Se añade la respuesta original y se generan otras 2
    this.respuestas.push(this.coloresReales);
    //¿Cómo alejarse para los niveles bajos? => Resuelto

    //Generar 2 rangos aleatorios de aproximación por las dos restantes respuestas
    // Entre más bajo sea el nivel más alto será el rango de aproximación

    //TODO: Que aumente el nivel de dificultad gradualmente
    let rango = this.getRango();
    console.log("nivel", this.nivel);

    console.log(rango);
    let rgs = [
      this.randomInRange(rango.min, rango.max),
      this.randomInRange(rango.min, rango.max),
      this.randomInRange(rango.min, rango.max)
    ];

    console.log(rgs);
    console.log(this.coloresReales);

    //Obtener el color dominante y mantenerlo, para no variar tanto los colores
    console.log("Color dominante", this.getDominante());
    let cDominante = this.getDominante();
    for (let i = 0; i < 3; i++) {
      let res;
      res = {
        r: Math.floor(this.generarAproximados(rgs[i], this.coloresReales.r)),
        g: Math.floor(this.generarAproximados(rgs[i], this.coloresReales.g)),
        b: Math.floor(this.generarAproximados(rgs[i], this.coloresReales.b))
      };

      //Mantener color dominate
      if (rgs[i] > 5) {
        if (cDominante == "r")
          res.r = Math.floor(this.generarAproximados(5, this.coloresReales.r));
        if (cDominante == "g")
          res.g = Math.floor(this.generarAproximados(5, this.coloresReales.g));
        if (cDominante == "b")
          res.b = Math.floor(this.generarAproximados(5, this.coloresReales.b));
      }
      this.respuestas.push(res);
    }
    console.log(this.respuestas);
    this.respuestas = this.shuffle(this.respuestas);
  }

  getDominante() {
    let c = Object.assign({}, this.coloresReales);
    if (c.r > c.g && c.r > c.b) return "r";
    if (c.g > c.r && c.g > c.b) return "g";
    if (c.b > c.g && c.b > c.r) return "b";
    return "g";
  }

  generarAproximados(errorRelativo, exac) {
    let aprox;
    //TODO: Revisar formular para calcular los aproximados

    let mas = Math.random() > 0.5;
    // aprox = (errorRelativo * 255) / 100;

    if (mas) {
      //   aprox = aprox + exac;
      aprox = (errorRelativo * exac) / 100 + exac;
    } else {
      //   aprox = aprox - exac;
      aprox = (errorRelativo * exac) / 100 - exac;
    }

    if (aprox > 255) aprox = exac - aprox;
    aprox = Math.abs(aprox);

    return aprox;
  }

  randomInRange(min: string | number, max: string | number) {
    min = parseInt(min + "");
    max = parseInt(max + "");
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }

  comprobar(r) {
    console.log(r);

    if (this.coloresReales == r) {
      this.puntuacion += 1;
      this.rachaActual += 1;
      if (this.rachaMax < this.rachaActual) {
        this.rachaMax = this.rachaActual;
        this.datosJuego.rachaMax = this.rachaMax;
      }
      this.audioCorrect.currentTime = 0;
      this.audioCorrect.play();
    } else {
      this.vidas -= 1;
      this.vibration.vibrate(400);
      this.rachaActual = 0;
      this.audioIncorrect.currentTime = 0;
      this.audioIncorrect.play();
    }

    //terminar juego
    if (this.vidas == 0) {
      this.terminarIntento();
    } else {
      //generar nuevo
      this.nivel += 1;
      this.randomColor();
      this.generarRespuestas();
    }
  }

  iniciar() {
    this.reiniciarParametros();
    this.intervalo = setInterval(() => {
      this.controlTiempo();
    }, 1000);
    this.conteo = true;
    this.audioConteo.currentTime = 0;
    this.randomColor();
    this.generarRespuestas();
    this.audioConteo.play();
  }

  reiniciarParametros() {
    this.finTiempo = false;
    this.segundos = this.tiempoInicial;
    this.tiempoHecho = 0;
    this.ultimosSegundos = false;
    this.tiempoString = this.parseTiempo(this.segundos);
    this.vidas = 3;
    this.puntuacion = 0;
    this.rachaActual = 0;
    this.nivel = 0;
  }

  controlTiempo() {
    if (this.segundos > 0) {
      this.tiempoHecho += 1;

      this.segundos -= 1;
      this.tiempoString = this.parseTiempo(this.segundos);

      if (this.segundos == this.tiempoInicial - 3) {
        this.conteo = false;
        if (!this.jugando) {
          this.jugando = true;
        } else {
          this.finIntento = false;
        }
      }

      if (this.segundos == 30) {
        this.ultimosSegundos = true;
        this.audio30Secods.currentTime = 0;
        this.audio30Secods.play();
      }
    } else {
      this.finTiempo = true;
      this.terminarIntento();
    }
  }

  terminarIntento() {
    clearInterval(this.intervalo);
    this.tiempoHecho -= 3;
    let cantProblemas = this.puntuacion + (3 - this.vidas);
    if (cantProblemas != 0) {
      this.promXproblema = parseFloat(
        (this.tiempoHecho / cantProblemas).toFixed(2)
      );
    } else {
      this.promXproblema = 0;
    }

    console.log(this.puntuacion);
    console.log(this.datosJuego.record);

    if (this.puntuacion > this.datosJuego.record) {
      console.log(this.puntuacion);
      console.log(this.datosJuego.record);
      console.log("Nuevo record");

      //Guardar nuevo record
      this.usuarioDB.recordR = this.puntuacion;
      this.updateRecord();
      this.audioFinBueno.play();
      this.nuevoRecord = true;
      this.datosJuego.record = this.puntuacion;
    } else {
      this.audioFinMalo.play();
      this.nuevoRecord = false;
    }

    this.datosJuego.intentos += 1;
    this.datosJuego.puntosAcum += this.puntuacion;
    this.finIntento = true;
  }

  toArray(cantidad: number) {
    var arreglo = [];
    for (let i = 0; i < cantidad; i++) {
      arreglo.push("");
    }
    return arreglo;
  }

  parseTiempo(segundos: number) {
    let minString: string = segundos / 60 + "";
    let min = parseInt(minString);
    let seg = segundos % 60;

    let resultado = "";

    if (seg < 10) {
      resultado = `${min}:0${seg}`;
    } else {
      resultado = `${min}:${seg}`;
    }

    return resultado;
  }

  shuffle(a: any[] | string[]) {
    for (let i = a.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      [a[i], a[j]] = [a[j], a[i]];
    }
    return a;
  }

  //TODO: Actualizar record del usuario
  updateRecord() {
    console.log("Actualizando datos");
    console.log(this.usuarioDB);

    let usuario = Object.assign({}, this.usuarioDB);
    delete usuario.id;
    this.api.update_User(this.usuarioDB.id, usuario);
  }

  verRanking() {}
}
