import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RushPage } from './rush.page';

describe('RushPage', () => {
  let component: RushPage;
  let fixture: ComponentFixture<RushPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RushPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RushPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
