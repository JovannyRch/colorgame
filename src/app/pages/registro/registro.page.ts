import { Component, OnInit } from "@angular/core";
import { ApiService } from "../../services/api.service";
import { ToastController } from "@ionic/angular";
import { Storage } from "@ionic/storage";
import { Router } from "@angular/router";

@Component({
  selector: "app-registro",
  templateUrl: "./registro.page.html",
  styleUrls: ["./registro.page.scss"]
})
export class RegistroPage implements OnInit {
  constructor(
    private api: ApiService,
    public toastController: ToastController,
    public storage: Storage,
    private router: Router
  ) { }
  nombreUsuario: string = "";
  isRegistrando: boolean = false;
  observador: number = 0;
  intervalo: any;
  ngOnInit() {
    this.intervalo = setInterval(() => {
      this.reloj();
    }, 1500);
  }

  bgColor: any = {
    r: 0,
    g: 0,
    b: 0,
  }

  async mostrarMensaje(mensaje: string) {
    const toast = await this.toastController.create({
      message: mensaje,
      duration: 2000
    });
    toast.present();
  }

  reloj() {
    this.bgColor = this.randomColor();
  }

  //TODO: hacer el registro de usuario
  registrarse() {
    if (this.nombreUsuario) {
      //TODO: validar usuario

      this.observador = 0;

      this.isRegistrando = true;

      /*
      this.conf.usuario = this.username;
    this.storage.set("conf", this.conf).then(
      ok=> {
        this.router.navigate(["/home/inicio"]);
      }
    )

    */

      let users = this.api.getUsuario(this.nombreUsuario);

      users.forEach(user => {
        if (user.length == 0) {
          this.hacerRegistro();
        } else {
          if (this.observador == 0) {
            this.isRegistrando = false;
            this.mostrarMensaje("Nombre de usuario repetido, intento con otro");
          }
        }
        this.observador += 1;
      });
    } else {
      this.mostrarMensaje("Ingrese nombre de usuario");
    }
  }

  hacerRegistro() {
    let usuario = {
      username: this.nombreUsuario,
      recordR: 0,
      recordC: 0,
      recordM: 0,
      intentosR: 0,
      intentosC: 0,
      intentosM: 0,
      puntosR: 0,
      puntosC: 0
    };
    this.api
      .create_NewUser(usuario)
      .then(response => {
        this.isRegistrando = false;
        this.mostrarMensaje("¡Registro exitoso!");
        this.guardarDatosUsuario();
      })
      .catch(e => {
        console.log("Ocurrió un error al hacer le registro de usuario");
        this.isRegistrando = false;
      });
  }

  guardarDatosUsuario() {
    let usuario = {
      username: this.nombreUsuario
    };
    this.storage.set("usuario", usuario).then(ok => {
      this.router.navigate(["/menu1"]);
    });
  }

  getRGB(color, i) {
    return `rgb(${color.r}, ${color.g}, ${color.b},${i})`;
  }
  randomColor() {
    let r = {
      r: 0,
      g: 0,
      b: 0
    };
    r.r = Math.floor(Math.random() * 255);
    r.g = Math.floor(Math.random() * 255);
    r.b = Math.floor(Math.random() * 255);
    return r;
  }
}
