import { Component, OnInit } from "@angular/core";
import { ApiService } from "../../services/api.service";
import { ActivatedRoute } from "@angular/router";
import { Storage } from "@ionic/storage";
import { Router } from "@angular/router";
import { Vibration } from "@ionic-native/vibration/ngx";
@Component({
  selector: "app-sala",
  templateUrl: "./sala.page.html",
  styleUrls: ["./sala.page.scss"]
})
export class SalaPage implements OnInit {
  constructor(
    private api: ApiService,
    private route: ActivatedRoute,
    private storage: Storage,
    private router: Router,
    public vibration: Vibration
  ) {}

  salaActual: any = {
    id: "",
    nombre: "123"
  };

  usuarioStorage: any = {
    username: "",
    record: null
  };

  isLoading: boolean = true;

  soyCreador: boolean = false;

  coloresReales: any = {
    r: 0,
    g: 0,
    b: 0
  };
  respuestas: any[];
  uniendose: boolean = false;
  nivel: number = 0;

  preguntaActual: number = 0;
  puntuacion: number = 0;
  vidas: number = 3;

  marcador: any = [];

  finPartida: boolean = false;
  sinVidas: boolean = false;
  audioCorrect = new Audio("assets/audio/correcto.mp3");
  audioIncorrect = new Audio("assets/audio/incorrecto.mp3");

  toArray(cantidad: number) {
    var arreglo = [];
    for (let i = 0; i < cantidad; i++) {
      arreglo.push("");
    }
    return arreglo;
  }

  getRango() {
    if (this.nivel < 5) {
      return {
        min: 20,
        max: 60
      };
    }

    if (this.nivel < 11) {
      return {
        min: 18,
        max: 45
      };
    }

    if (this.nivel < 20) {
      return {
        min: 17,
        max: 37
      };
    }

    if (this.nivel < 25) {
      return {
        min: 12,
        max: 20
      };
    }
    if (this.nivel < 30) {
      return {
        min: 5,
        max: 15
      };
    }

    if (this.nivel < 35) {
      return {
        min: 5,
        max: 10
      };
    }

    if (this.nivel < 40) {
      return {
        min: 2,
        max: 7
      };
    }

    if (this.nivel < 45) {
      return {
        min: 2,
        max: 5
      };
    }
    return {
      min: 1,
      max: 3
    };
  }

  totalPreguntas: number = 10;

  generarRespuestas() {
    // Entre mas fácil sea el nivel, mas lejano deben estar los valores de las posibles respuestas
    this.respuestas = [];
    //Se añade la respuesta original y se generan otras 2
    this.respuestas.push(this.coloresReales);
    //¿Cómo alejarse para los niveles bajos? => Resuelto

    //Generar 2 rangos aleatorios de aproximación por las dos restantes respuestas
    // Entre más bajo sea el nivel más alto será el rango de aproximación

    //TODO: Que aumente el nivel de dificultad gradualmente
    let rango = this.getRango();
    console.log("nivel", this.nivel);
    rango.min = 1;
    rango.max = 20;

    console.log(rango);
    let rgs = [
      this.randomInRange(rango.min, rango.max),
      this.randomInRange(rango.min, rango.max),
      this.randomInRange(rango.min, rango.max)
    ];

    console.log(rgs);
    console.log(this.coloresReales);

    //Obtener el color dominante y mantenerlo, para no variar tanto los colores
    console.log("Color dominante", this.getDominante());
    let cDominante = this.getDominante();
    for (let i = 0; i < 3; i++) {
      let res;
      res = {
        r: Math.floor(this.generarAproximados(rgs[i], this.coloresReales.r)),
        g: Math.floor(this.generarAproximados(rgs[i], this.coloresReales.g)),
        b: Math.floor(this.generarAproximados(rgs[i], this.coloresReales.b))
      };

      //Mantener color dominate
      if (rgs[i] > 5) {
        if (cDominante == "r")
          res.r = Math.floor(this.generarAproximados(5, this.coloresReales.r));
        if (cDominante == "g")
          res.g = Math.floor(this.generarAproximados(5, this.coloresReales.g));
        if (cDominante == "b")
          res.b = Math.floor(this.generarAproximados(5, this.coloresReales.b));
      }
      this.respuestas.push(res);
    }
    //console.log(this.respuestas);
    this.respuestas = this.shuffle(this.respuestas);
  }

  getDominante() {
    let c = Object.assign({}, this.coloresReales);
    if (c.r > c.g && c.r > c.b) return "r";
    if (c.g > c.r && c.g > c.b) return "g";
    if (c.b > c.g && c.b > c.r) return "b";
    return "g";
  }

  generarAproximados(errorRelativo, exac) {
    let aprox;
    //TODO: Revisar formular para calcular los aproximados

    let mas = Math.random() > 0.5;
    // aprox = (errorRelativo * 255) / 100;

    if (mas) {
      //   aprox = aprox + exac;
      aprox = (errorRelativo * exac) / 100 + exac;
    } else {
      //   aprox = aprox - exac;
      aprox = (errorRelativo * exac) / 100 - exac;
    }

    if (aprox > 255) aprox = exac - aprox;
    aprox = Math.abs(aprox);

    return aprox;
  }

  randomInRange(min: string | number, max: string | number) {
    min = parseInt(min + "");
    max = parseInt(max + "");
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }

  shuffle(a: any[] | string[]) {
    for (let i = a.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      [a[i], a[j]] = [a[j], a[i]];
    }
    return a;
  }

  ngOnInit() {
    this.isLoading = true;
    this.salaActual.id = this.route.snapshot.paramMap.get("sala");
    // datos del usuario
    this.storage.get("usuario").then(val => {
      if (val) {
        this.usuarioStorage = val;
        this.getSala(this.salaActual.id);
      }
      console.log(val);
    });
  }

  getSala(id) {
    this.api.get("salas").subscribe(data => {
      data.map(e => {
        let data = e.payload.doc.data();
        if (id == e.payload.doc.id) {
          console.log("data", data);

          let sala = {
            id: e.payload.doc.id,
            creador: data["creador"],
            jugadores: data["jugadores"],
            terminado: data["terminado"],
            iniciado: data["iniciado"],
            nombre: data["nombre"],
            puzzles: data["puzzles"],
            puntuaciones: data["puntuaciones"],
            cantidad: data["cantidad"],
            reiniciar: data["reiniciar"]
          };

          this.totalPreguntas = sala.cantidad;
          if (sala.puzzles.length > 0) {
            this.coloresReales = sala.puzzles[0].pregunta;
            this.respuestas = sala.puzzles[0].respuestas;
          }
          if (sala.reiniciar && sala.iniciado) {
            this.volverAJugar();
            if (this.soyCreador) {
              this.salaActual.iniciado = false;
              this.salaActual.puntuaciones = [];
              this.salaActual.reiniciar = false;
              this.updateSala();
            }
          }

          if (sala.puntuaciones.length > 0) {
            //Ordenar puntuaciones
            this.marcador = sala.puntuaciones.sort((a, b) => {
              return b.puntuacion - a.puntuacion;
            });

            console.log(this.marcador);
          }

          this.salaActual = sala;

          console.log("Sala actual", this.salaActual);
          if (this.salaActual.creador == this.usuarioStorage.username) {
            //SOy el creador de la sala
            console.log("Soy el creador");

            this.soyCreador = true;
          } else {
            console.log(
              "Estoy en la sala?",
              this.salaActual.jugadores.includes(this.usuarioStorage.username)
            );

            if (
              this.salaActual.jugadores.includes(this.usuarioStorage.username)
            ) {
              console.log("Ya estoy inscrito");
            } else {
              //incribirme a la sala
              console.log("Agregando a la sala, 2");
              this.salaActual.jugadores.push(this.usuarioStorage.username);
              this.updateSala();
            }
            this.isLoading = false;
            return;
          }
          this.isLoading = false;
          return;
        }

        this.isLoading = false;
      });
    });
  }

  updateSala() {
    console.log("Actualizando sala");

    let sala = Object.assign({}, this.salaActual);
    delete sala.id;
    this.api.update("salas", this.salaActual.id, sala);
  }

  iniciar() {
    //generar n preguntas
    let puzzles = [];
    this.nivel = 25;
    for (let i = 0; i < this.totalPreguntas; i++) {
      this.coloresReales = this.randomColor2();
      this.generarRespuestas();
      this.nivel += 1;
      puzzles.push({
        pregunta: this.coloresReales,
        respuestas: this.respuestas
      });
    }
    console.log(puzzles);

    this.salaActual.iniciado = true;

    this.salaActual.puzzles = puzzles;
    this.updateSala();
  }

  randomColor() {
    this.coloresReales.r = Math.floor(Math.random() * 255);
    this.coloresReales.g = Math.floor(Math.random() * 255);
    this.coloresReales.b = Math.floor(Math.random() * 255);
  }

  randomColor2() {
    let r = { r: 0, g: 0, b: 0 };
    r.r = Math.floor(Math.random() * 255);
    r.g = Math.floor(Math.random() * 255);
    r.b = Math.floor(Math.random() * 255);
    return r;
  }

  getRGB(color) {
    return `rgb(${color.r}, ${color.g}, ${color.b})`;
  }
  comprobar(r) {
    if (
      this.coloresReales.r == r.r &&
      this.coloresReales.g == r.g &&
      this.coloresReales.b == r.b
    ) {
      this.puntuacion += 1;
      this.audioCorrect.currentTime = 0;
      this.audioCorrect.play();
    } else {
      this.vidas -= 1;
      this.vibration.vibrate(400);
      this.audioIncorrect.currentTime = 0;
      this.audioIncorrect.play();
    }

    //terminar juego
    if (this.vidas == 0 || this.preguntaActual + 1 == this.totalPreguntas) {
      if (this.vidas == 0) this.sinVidas = true;

      this.terminarIntento();
    } else {
      //generar nuevo
      this.preguntaActual += 1;
      this.coloresReales = this.salaActual.puzzles[
        this.preguntaActual
      ].pregunta;
      this.respuestas = this.salaActual.puzzles[this.preguntaActual].respuestas;
    }
  }

  terminarIntento() {
    this.finPartida = true;
    this.salaActual.puntuaciones.push({
      username: this.usuarioStorage.username,
      puntuacion: this.puntuacion
    });
    this.updateSala();
  }

  reiniciar() {
    this.finPartida = false;
    this.vidas = 3;
    this.puntuacion = 0;
    this.sinVidas = false;
    this.preguntaActual = 0;
  }

  volverAJugar() {
    this.reiniciar();
    if (this.soyCreador) {
      this.iniciar();
    }
  }

  onReset() {
    this.salaActual.reiniciar = true;
    this.updateSala();
  }

  generarPreguntaRush() {}
}
