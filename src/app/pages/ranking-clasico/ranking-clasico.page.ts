import { Component, OnInit } from "@angular/core";

import { ApiService } from "../../services/api.service";

import { ActivatedRoute } from "@angular/router";

@Component({
  selector: "app-ranking-clasico",
  templateUrl: "./ranking-clasico.page.html",
  styleUrls: ["./ranking-clasico.page.scss"]
})
export class RankingClasicoPage implements OnInit {
  constructor(private api: ApiService, private route: ActivatedRoute) {}

  jugadores: any[] = [];
  cargando: boolean = true;
  //Pagina de la que viene
  pageBefore: string;
  ngOnInit() {
    this.pageBefore = this.route.snapshot.paramMap.get("page");
    this.getRanking();
  }

  getRanking() {
    console.log("cargando datos");

    this.cargando = true;
    this.api.getRankinkgRush("recordC", "desc").subscribe(data => {
      console.log(data);

      this.jugadores = data.map(e => {
        return {
          id: e.payload.doc.id,
          username: e.payload.doc.data()["username"],
          recordC: e.payload.doc.data()["recordC"]
        };
      });
      this.cargando = false;
      console.log(this.jugadores);
    });
  }
}
