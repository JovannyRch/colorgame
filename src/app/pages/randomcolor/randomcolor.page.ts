import { Component, OnInit, OnDestroy } from "@angular/core";

@Component({
  selector: "app-randomcolor",
  templateUrl: "./randomcolor.page.html",
  styleUrls: ["./randomcolor.page.scss"]
})
export class RandomcolorPage implements OnInit,OnDestroy {
  constructor() {}

  color: any = {
    r: 0,
    g: 0,
    b: 0
  };
  segundos: number = 0;
  intervalo: any;
  isStopped: boolean = true;

  ngOnDestroy(){
  
    clearInterval(this.intervalo);
  }
  
  ngOnInit() {
    this.randomColor();
    console.log(this.color);
    this.stopOrPlay();
  }

  controlTiempo(){
    this.randomColor();
  }

  randomColor() {
    this.color.r = Math.floor(Math.random() * 255);
    this.color.g = Math.floor(Math.random() * 255);
    this.color.b = Math.floor(Math.random() * 255);
  }

  toRGB(l) {
    return `rgb(${this.color.r}, ${this.color.g}, ${this.color.b},${l})`;
  }

  stopOrPlay(){
    this.isStopped = !this.isStopped;
    if(this.isStopped){
      clearInterval(this.intervalo)       
    }else{
      this.intervalo = setInterval(
        ()=> {
          this.randomColor();
        },
        1000
      );
    }
  }


}
