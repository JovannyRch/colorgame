import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { RandomcolorPage } from './randomcolor.page';

const routes: Routes = [
  {
    path: '',
    component: RandomcolorPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [RandomcolorPage]
})
export class RandomcolorPageModule {}
