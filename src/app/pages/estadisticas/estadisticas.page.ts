import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Chart } from 'chart.js';
import { ActionSheetController } from '@ionic/angular';


@Component({
  selector: 'app-estadisticas',
  templateUrl: './estadisticas.page.html',
  styleUrls: ['./estadisticas.page.scss'],
})
export class EstadisticasPage implements OnInit {

  @ViewChild("doughnutCanvas", { static: false }) doughnutCanvas: ElementRef;

  ngOnInit() {
    // Configuración del juego
    this.storage.get('conf').then((val) => {
      if (val) this.conf = val;
      else this.storage.set('conf', this.conf);
    });
    this.getStatsGenerales();


  }

  private doughnutChart: Chart;


  datosModo: any = {
    record: 0,
    intentos: 0,
    puntosAcum: 0,
    rachaMax: 0,
    tiempoAcum: 0,
    ejerciciosAcum: 0
  };

  datosGenerales: any = {
    intentos: 0,
    correctos: 0,
    fallos: 0,
    tiempoJugado: 0
  };

  isGeneral: any = true;

  modos: any[] = [
    { id: -1, label: {1: 'General' , 2:"General"}},
    { id: 0, label: {1: 'Suma', 2: "Addition"} },
    { id: 1, label: {1: 'Resta', 2: "Subtraction"} },
    { id: 2, label: {1: 'Multiplicación', 2: "Multiplication"} },
    { id: 3, label: {1: 'División', 2: "Division"} },
    { id: 4, label: {1:'Todos los modos', 2: "All modes"} },
    { id: 5, label: {1: 'Suma y resta', 2: "Addition and subtraction"} },
    { id: 6, label: {1: 'Multiplicación y división', 2: "Multiplication and division"} },
  ];

  modoSeleccionado: any = { id: -1, label: "General" };

  //Dificultad
  dificultades: any = [
    {id: 0,label: {1: 'Fácil', 2: "Easy"}},
    {id: 1,label: {1: 'Normal', 2: "Normal"}},
    {id: 2,label: {1: 'Difícil', 2: "Hard"}},
  ]

  conf: any = {
    dificultad: 1,
    tiempo: 1,
    temaOscuro: false,
    dosJugadores: false,
    idioma: null
  };

  mensajes: any = {
    titulo: {1: "Estadísticas", 2: "Stats"},
    general: {1: "General", 2: "General"},
    puntajePromedio: {1: "Puntaje Promedio", 2: "Avg Score"},
    tiempoJugado: {1: "Tiempo Jugado", 2: "Time Played"},
    intentos: {1: "Juegos jugados", 2: "Games Played"},
    aciertos: {1: "Aciertos", 2: "Corrects"},
    fallos: {1: "Fallos", 2: "Incorrects"},
    seleccionarModo: {1: "Seleccionar Modo", 2: "Choose Mode"},
    tiempoPromedio: {1: "Tiempo Promedio por Problema", 2: "Avg Time Per Problem"},
    reiniciar: {1: "Reiniciar estadisticas", 2:"Reset Stats"},
    noData: {1: "Sin datos", 2: "No data"},
    record: {1: "Mayor puntuación", 2: "Record"},
    mayorRacha: {1: "Mayor racha", 2: "Longest Strek"},
    puntos: {1: "Puntos totales", 2: "Total Score"},
    correctos: {1: "Corrects", 2: "Corrects"},
    errores: {1: "Errores", 2: "Fails"},
    confirmacion: {1: "Confirmación", 2: "Confirmation"},
    reset: {1: "Reiniciar estadísticas", 2: "Reset stats"},
    cancelar: {1: "Cacelar", 2: "Cancel"}
  };


  stats: any = {
    0: {
      0: [],
      1: [],
      2: [],
    },
    1: {
      0: [],
      1: [],
      2: [],
    },
    2: {
      0: [],
      1: [],
      2: [],
    },
  };


  //Modo
  tiempos: any = [
    {id: 0,label: {1: '1 Minuto', 2: "1 Minute"}},
    {id: 1,label:  {1: '2 Minutos', 2: "2 Minutes"}},
    {id: 2,label:  {1: '3 Minutos', 2: "3 Minutes"}}
   // {id: 2,label: 'Sin tiempo '}
  ];

  tiempoSeleccionado = -1;

  labelModo = "General";

  constructor(private storage: Storage, public actionSheetController: ActionSheetController) { }


  async menuReiniciarDatos() {
    const actionSheet = await this.actionSheetController.create({
      header: this.mensajes.confirmacion[this.conf.idioma],
      buttons: [{
        text: this.mensajes.reset[this.conf.idioma],
        role: 'destructive',
        icon: 'trash',
        handler: () => {
          for (let i = 0;i< this.modos.length -1;i++) {
        
            for (const dificultad of this.dificultades) {
              for (const tiempo of this.tiempos) {
             
                this.storage.set('data' +(i+1) + '/' + dificultad.id + '/' + tiempo.id, {
                  record: 0,
                  intentos: 0,
                  puntosAcum: 0,
                  rachaMax: 0,
                  tiempoAcum: 0,
                  ejerciciosAcum: 0,
                  fallos: 0,
                  aciertos: 0
                });
              }
            }
          };

          //reiniciar datos generales
          this.storage.set('data', {
            intentos: 0,
            correctos: 0,
            fallos: 0,
            tiempoJugado: 0
          });

          setTimeout(() => {
            this.getStatsGenerales();
          }, 300);
          
        }
      },
      {
        text: this.mensajes.cancelar[this.conf.idioma],
        role: 'cancel',
        icon: 'close',
        handler: () => {
          console.log('Cancel clicked');
        }
      }
      ]
    });
    await actionSheet.present();
  }

  ngAfterViewInit() {

  }

  crearGrafica(data) {
    let aciertos = "";
    let errores = "";
    if(this.conf.idioma == 1){
      errores = "Errores";
      aciertos = "Aciertos";
    }
    else{
      aciertos = "Corrects";
      errores = "Fails"
    }


    if (this.doughnutCanvas)
      this.doughnutChart = new Chart(this.doughnutCanvas.nativeElement, {
        type: "doughnut",
        data: {
          labels: [errores, aciertos],
          datasets: [
            {
              data: data,
              backgroundColor: [
                "#f37915",
                "black",
              ]
            }
          ]
        }
      });
  }

  getStatsGenerales() {
    this.storage.get('data').then((val) => {
      if (val) {
        this.datosGenerales = val;
        let data = [];
        data.push(this.datosGenerales.correctos);
        data.push(this.datosGenerales.fallos);
        this.crearGrafica(data)
      }
    });
  }

  selecionarModo($event) {
    if (this.modoSeleccionado == -1) {
      this.isGeneral = true;
      this.labelModo = "General";
      this.getStatsGenerales();

    }
    else {
      this.reiniciarStats();
      for (const dificultad of this.dificultades) {
        for (const tiempo of this.tiempos) {
          this.storage.get('data' + (this.modoSeleccionado + 1) + '/' + dificultad.id + '/' + tiempo.id).then((val) => {

            this.stats[dificultad.id][tiempo.id] = val;

          });
        }
      }

      this.isGeneral = false;
      this.labelModo = this.modos[this.modoSeleccionado + 1].label[this.conf.idioma];
    }
  }

  validarStats(d, t) {
    if (this.stats[d][t]) {
      return true;
    } else {
      return false;
    }
  }

  reiniciarStats() {
    this.stats = {
      0: {
        0: null,
        1: null,
        2: null,
      },
      1: {
        0: null,
        1: null,
        2: null,
      },
      2: {
        0: null,
        1: null,
        2: null,
      },
    };
  }

  promedioTiempoXProblema(tiempo: number, correctos: number, errores: number) {
    let promedio = (tiempo / (correctos + errores)).toFixed(2);
    return promedio;
  }

  //Calcular el porcentaje
  calcPorcentaje(total: number, valor: number) {
    let porcentaje = ((valor * 100) / total).toFixed(2);
    return porcentaje;
  }

  promedioPuntajeXIntento(intentos: number, correctos: number, errores: number) {

    let promedio: string = "0";
    if (intentos > 0) {
      promedio = ((correctos + errores) / intentos).toFixed(2);
    }
    return promedio;
  }

  promedioPuntajeXIntento2(puntos: number, intentos: number) {
    let promedio: string = "0";
    if (intentos > 0) {
      promedio = (puntos / intentos).toFixed(2);
    }
    return promedio;
  }

  parseSeconds(seconds: number) {
    let resultado: string = "";
    let dias = parseInt((seconds / 86400) + "");
    if (dias > 0) {
      if (dias == 1) {
        if(this.conf.idioma == 1)resultado += `${dias} día `;
        else resultado += `${dias} day `;
      } else {
        if(this.conf.idioma == 1)resultado += `${dias} días `;
        else resultado += `${dias} days `;
      }
    }
    //Segundos restantes
    seconds = seconds % 86400;
    let horas = parseInt((seconds / 3600) + "");
    if (horas > 0) {
      if (horas == 1) {
        if(this.conf.idioma == 1)resultado += `${dias} hora `;
        else resultado += `${dias} hour `;
      } else {
        if(this.conf.idioma == 1)resultado += `${dias} horas `;
        else resultado += `${dias} hours `;
      }
    }

    //Segundos restantes
    seconds = seconds % 3600;
    let minutos = parseInt((seconds / 60) + "");
    if (seconds % 60 >= 30) minutos += 1;
    if (minutos > 0) {
      if (minutos == 1) {
        if(this.conf.idioma == 1) resultado += `${minutos} minuto `;
        else resultado += `${minutos} minute `;
      } else {
        if(this.conf.idioma == 1) resultado += `${minutos} minutos `;
        else resultado += `${minutos} minutes `;
      }
    }

    return resultado;

  }


}
