import { Component, OnInit } from "@angular/core";
import { ApiService } from "../../services/api.service";
@Component({
  selector: "app-clasico",
  templateUrl: "./clasico.page.html",
  styleUrls: ["./clasico.page.scss"]
})
export class ClasicoPage implements OnInit {
  constructor(private api: ApiService) {}

  coloresReales: any = {
    r: 0,
    g: 0,
    b: 0
  };

  coloresUsuario: any = {
    r: 0,
    g: 0,
    b: 0
  };

  porcen: any = {
    r: 0,
    g: 0,
    b: 0
  };

  porcentajeTotal: any = "";

  isVerificar: boolean = false;

  siguienteColor: any = {
    r: 0,
    g: 0,
    b: 0
  };

  errorTotal: number = 0;

  ayudas: number = 0;

  nivel: number = 0;

  getValores() {
    if (this.nivel == 1) {
      return {
        valores: [100, 200],
        colores: 1
      };
    }

    if (this.nivel < 2) {
      return {
        valores: [100, 200],
        colores: 2
      };
    }

    if (this.nivel < 3) {
      return {
        valores: [100, 200],
        colores: 2
      };
    }

    if (this.nivel < 6) {
      return {
        valores: [75, 125, 150, 230],
        colores: 2
      };
    }

    if (this.nivel < 9) {
      return {
        valores: [75, 125, 255],
        colores: 3
      };
    }

    if (this.nivel <= 15) {
      return {
        valores: [25, 50, 75, 100, 125, 150, 175, 200, 225, 255],
        colores: 3
      };
    }
    return {
      valores: [15, 45, 25, 65, 50, 75, 100, 115, 125, 150, 175, 200, 225, 255],
      colores: 3
    };
  }

  valores: number[] = [];

  ngOnInit() {
    this.coloresReales = this.randomColor(true);
    this.siguienteColor = this.randomColor(false);

    this.valores.push(this.coloresReales.r);
    this.valores.push(this.coloresReales.g);
    this.valores.push(this.coloresReales.b);
  }

  getDominante() {
    let c = Object.assign({}, this.coloresReales);
    if (c.r > c.g && c.r > c.b) return "r";
    if (c.g > c.r && c.g > c.b) return "g";
    if (c.b > c.g && c.b > c.r) return "b";
    return "g";
  }

  randomInRange(min: string | number, max: string | number) {
    min = parseInt(min + "");
    max = parseInt(max + "");
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }

  toStringValores() {
    let res = "";
    for (let i = 0; i < this.valores.length; i++) {
      const element = this.valores[i];
      if (i + 1 == this.valores.length) {
        res += element;
      } else {
        res += element + ", ";
      }
    }
    return res;
  }

  randomColor(aumentarNivel) {
    let r = {
      r: 0,
      g: 0,
      b: 0
    };

    if (aumentarNivel) {
      this.nivel += 1;
    }
    let conf = this.getValores();

    console.log("nivel", this.nivel);
    console.log("conf", conf);

    if (conf) {
      let colores = ["r", "g", "b"];

      if (conf.colores == 1) {
        let rand = colores[Math.floor(Math.random() * colores.length)];
        r[rand] = this.randomInRange(0, 255);
      } else if (conf.colores == 2) {
        let rand = colores[Math.floor(Math.random() * colores.length)];
        r[rand] = this.randomInRange(0, 255);
        colores.splice(colores.indexOf(rand), 1);
        rand = colores[Math.floor(Math.random() * colores.length)];
        r[rand] = this.randomInRange(0, 255);
      } else {
        r.r = this.randomInRange(0, 255);
        r.g = this.randomInRange(0, 255);
        r.b = this.randomInRange(0, 255);

        r.g = this.randomInRange(
          r.g - 20 < 0 ? 0 : r.g - 20,
          r.g + 20 > 255 ? 255 : r.g + 20
        );

        r.b = this.randomInRange(
          r.b - 20 < 0 ? 0 : r.b - 20,
          r.b + 20 > 255 ? 255 : r.b + 20
        );

        r.r = this.randomInRange(
          r.r - 20 < 0 ? 0 : r.r - 20,
          r.r + 20 > 255 ? 255 : r.r + 20
        );
      }
    } else {
      r.r = Math.floor(Math.random() * 255);
      r.g = Math.floor(Math.random() * 255);
      r.b = Math.floor(Math.random() * 255);
    }
    return r;
  }

  nextColor() {
    this.siguienteColor.r = Math.floor(Math.random() * 255);
    this.siguienteColor.g = Math.floor(Math.random() * 255);
    this.siguienteColor.b = Math.floor(Math.random() * 255);
  }

  getRGB(color) {
    return `rgb(${color.r}, ${color.g}, ${color.b})`;
  }

  getRGBLight(color) {
    return `rgb(${color.r}, ${color.g}, ${color.b},0.1)`;
  }
  validar() {
    this.porcen.r = this.porcentajeError(
      this.coloresUsuario.r,
      this.coloresReales.r
    );
    this.porcen.g = this.porcentajeError(
      this.coloresUsuario.g,
      this.coloresReales.g
    );
    this.porcen.b = this.porcentajeError(
      this.coloresUsuario.b,
      this.coloresReales.b
    );
    this.porcentajeTotal = (
      (parseInt(this.porcen.r) +
        parseInt(this.porcen.g) +
        parseInt(this.porcen.b)) /
      3
    ).toFixed(2);

    this.isVerificar = true;
  }

  porcentajeError(aprox, exac) {
    if (exac == 0) {
      exac = exac + 1;
      aprox = aprox + 1;
    }

    let diferencia = Math.abs(aprox - exac);
    let porcen = (diferencia * 100) / 255;
    return porcen.toFixed(2);
  }

  ayuda() {
    this.ayudas += 1;
    if (this.ayudas <= 3) {
      let res = Object.assign({}, this.coloresReales);

      res.r = Math.floor(this.generarAproximados(10, this.coloresReales.r));
      res.g = Math.floor(this.generarAproximados(10, this.coloresReales.g));
      res.b = Math.floor(this.generarAproximados(10, this.coloresReales.b));
      let cDominante = this.getDominante();
      console.log("Dominante", cDominante);

      if (cDominante == "r") res.r = this.coloresReales.r;
      if (cDominante == "g") res.g = this.coloresReales.g;
      if (cDominante == "b") res.b = this.coloresReales.b;
      this.coloresUsuario = Object.assign({}, res);
      console.log("ayuda", this.coloresReales);
    }
  }

  generarAproximados(errorRelativo, exac) {
    let aprox = (errorRelativo * exac) / 100 + exac;
    if (aprox > 255) aprox = aprox % 255;
    return aprox;
  }

  siguiente() {
    this.coloresReales = Object.assign({}, this.siguienteColor);
    console.log(this.coloresReales);

    this.siguienteColor = this.randomColor(true);
  }

  reiniciar() {
    this.coloresReales = Object.assign({}, this.siguienteColor);
    this.valores = [];
    console.log("reiniciar");

    this.valores.push(this.coloresReales.r);
    this.valores.push(this.coloresReales.g);
    this.valores.push(this.coloresReales.b);

    //Meter valores basura para confundir
    if (this.nivel > 10) {
      for (let i = 0; i < 3; i++) {
        this.valores.push(this.randomInRange(1, 255));
      }
    }
    console.log("valores", this.valores);

    this.siguienteColor = this.randomColor(true);
    this.isVerificar = false;
    this.coloresUsuario.r = 0;
    this.coloresUsuario.g = 0;
    this.coloresUsuario.b = 0;
  }
}
