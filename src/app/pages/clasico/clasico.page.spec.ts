import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClasicoPage } from './clasico.page';

describe('ClasicoPage', () => {
  let component: ClasicoPage;
  let fixture: ComponentFixture<ClasicoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClasicoPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClasicoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
