import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NuevemillonesPage } from './nuevemillones.page';

describe('NuevemillonesPage', () => {
  let component: NuevemillonesPage;
  let fixture: ComponentFixture<NuevemillonesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NuevemillonesPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NuevemillonesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
