import { Component, OnInit, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-nuevemillones',
  templateUrl: './nuevemillones.page.html',
  styleUrls: ['./nuevemillones.page.scss'],
})
export class NuevemillonesPage implements OnInit {

  constructor() { }

  color: any = {
    r: 0,
    g: 0,
    b: 0,
  }

  intervalo: any;

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    clearInterval(this.intervalo);
    console.log("adios");

  }

  ngOnInit() {

    this.intervalo = setInterval(() => {
      this.controlTiempo();
    }, 1);


  }

  controlTiempo() {
    //console.log(this.color);

    this.color.r += 1;

    if (this.color.r == 255 && this.color.g == 255 && this.color.b == 255) {
      this.color = {
        r: 0,
        g: 0,
        b: 0
      }
    }

    if (this.color.r == 256) {
      this.color.g += 1;
      this.color.r = 0;
    }

    if (this.color.g == 256) {
      this.color.b += 1;
      this.color.g = 0;
    }

  }

  getRGB(color) {
    return `rgb(${color.r}, ${color.g}, ${color.b})`;
  }

}
