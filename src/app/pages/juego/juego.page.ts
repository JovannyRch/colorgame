import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { Storage } from "@ionic/storage";
//Vibración
import { Vibration } from "@ionic-native/vibration/ngx";

import { NivelesService } from "../../services/niveles.service";

@Component({
  selector: "app-juego",
  templateUrl: "./juego.page.html",
  styleUrls: ["./juego.page.scss"]
})
export class JuegoPage implements OnInit {
  constructor(
    private activeRoute: ActivatedRoute,
    private storage: Storage,
    public vibration: Vibration,
    private nivelesService: NivelesService
  ) {}

  pregunta: string;
  correcta: string;
  respuestas: string[] = [];

  historial: boolean[];
  historialPreguntas: string[];

  nivel: number = 0;
  puntuacion: number = 0;
  vidas: number = 3;

  //Parametros del juego
  segundos: number = 180;
  tiempoString: string = "";
  modo: number;
  dificultad: number;
  titulo: string;
  pregsXnvl: number;
  pregsXnvl_param: number = 0;

  tiempoInicial: number = 183;
  tipoTiempo: number = 0;
  intervalo: any;

  // En el modo principiante, guarda la pregunta anterior
  ansPregunta: string = "";

  datosJuego: any = {
    record: 0,
    intentos: 0,
    puntosAcum: 0,
    rachaMax: 0,
    tiempoAcum: 0,
    ejerciciosAcum: 0,
    fallos: 0,
    aciertos: 0
  };

  datosGenerales: any = {
    intentos: 0,
    correctos: 0,
    fallos: 0,
    tiempoJugado: 0
  };

  rachaActual: number = 0;
  rachaMax: number = 0;
  promXproblema: number = 0;
  tiempoHecho = 0;

  // Banderas
  jugando: boolean = false;
  finIntento: boolean = false;
  nuevoRecord: boolean = false;
  conteo: boolean = false;
  ultimosSegundos: boolean = false;

  // Rangos por nivel
  rangos: any;

  //Audios
  audioCorrect = new Audio("assets/audio/correcto.mp3");
  audioIncorrect = new Audio("assets/audio/incorrecto.mp3");
  audioConteo = new Audio("assets/audio/conteo.mp3");
  audio30Secods = new Audio("assets/audio/30seconds.webm");
  audioFinNormal = new Audio("assets/audio/resultado_normal.mp3");
  audioFinBueno = new Audio("assets/audio/resultado_bueno.mp3");
  audioFinMalo = new Audio("assets/audio/resultado_malo.mp3");

  conf: any = {
    dificultad: 1,
    tiempo: 1,
    temaOscuro: false,
    dosJugadores: false,
    idioma: null
  };

  mensajes: any = {
    mejorPuntuacion: { 1: "Tu mejor puntuación", 2: "Your record" },
    instrucciones: {
      1: "¡Resuelve todos los problemas que puedas!",
      2: "Solve all the problems you can!"
    },
    inicio: { 1: "Comenzar", 2: "Start" },
    terminar: { 1: "Rendirse", 2: "Quit" },
    endGame: { 1: "Fin del juego", 2: "Game Over" },
    puntuacion: { 1: "Puntuación", 2: "Score" },
    rachaMax: { 1: "Racha Más Larga", 2: "Longest Streak" },
    tiempoPromedio: { 1: "Tiempo Por Problema", 2: "Time Per Problem" },
    record: { 1: "Mejor puntuación", 2: "Record" },
    nuevoRecord: { 1: "¡Nuevo Record!", 2: "New Record!" }
  };

  modos: any = {
    1: { 1: "Suma", 2: "Addition", img: "assets/img/1.png" },
    2: { 1: "Resta", 2: "Subtraction", img: "assets/img/2.png" },
    3: { 1: "Multiplicación", 2: "Multiplication", img: "assets/img/3.png" },
    4: { 1: "División", 2: "Division", img: "assets/img/4.png" },
    5: { 1: "Todos los modos", 2: "All modes", img: "assets/img/5.png" },
    6: {
      1: "Suma y Resta",
      2: "Addition and Subtraction",
      img: "assets/img/6.png"
    },
    7: {
      1: "Multiplicación y División",
      2: "Multiplication and Division",
      img: "assets/img/7.png"
    }
  };

  tiempos: any = {
    0: { 1: "1 Minuto", 2: "1 Minute" },
    1: { 1: "2 Minutos", 2: "2 Minutes" },
    2: { 1: "3 Minutos", 2: "3 Minutes" }
    // {id: 2,label: 'Sin tiempo '}
  };

  ngOnInit() {
    // Configuración del juego
    this.storage.get("conf").then(val => {
      if (val) this.conf = val;
      else this.storage.set("conf", this.conf);
    });

    this.audioCorrect.load();
    this.audioIncorrect.load();

    this.modo = parseInt(this.activeRoute.snapshot.paramMap.get("modo"));
    this.dificultad = parseInt(
      this.activeRoute.snapshot.paramMap.get("dificultad")
    );
    this.tipoTiempo = parseInt(
      this.activeRoute.snapshot.paramMap.get("tiempo")
    );

    if (this.tipoTiempo == 0) {
      this.tiempoInicial = 63;
    }
    if (this.tipoTiempo == 1) {
      this.tiempoInicial = 123;
    }
    if (this.tipoTiempo == 2) {
      this.tiempoInicial = 183;
    }

    //this.guardarDatosGenerales();

    // Obtención del record por modo
    this.storage
      .get("data" + this.modo + "/" + this.dificultad + "/" + this.tipoTiempo)
      .then(val => {
        if (val) {
          this.datosJuego = val;
        } else {
          this.guardarDatos();
        }
      });

    // Datos generales del juego
    this.storage.get("data").then(val => {
      if (val) {
        this.datosGenerales = val;
      } else {
        this.guardarDatosGenerales();
      }
    });

    this.rangos = this.nivelesService.getRangos(this.modo);
    this.pregsXnvl = this.rangos[this.nivel][this.dificultad].cantidad;
  }

  controlTiempo() {
    if (this.segundos > 0) {
      this.tiempoHecho += 1;

      this.segundos -= 1;
      this.tiempoString = this.parseTiempo(this.segundos);

      if (this.segundos == this.tiempoInicial - 3) {
        this.conteo = false;
        if (!this.jugando) {
          this.jugando = true;
        } else {
          this.finIntento = false;
        }
      }

      if (this.segundos == 30) {
        this.ultimosSegundos = true;
        this.audio30Secods.currentTime = 0;
        this.audio30Secods.play();
      }
    } else {
      this.terminarIntento();
    }
  }

  guardarDatos() {
    this.storage.set(
      "data" + this.modo + "/" + this.dificultad + "/" + this.tipoTiempo,
      this.datosJuego
    );
  }

  guardarDatosGenerales() {
    this.storage.set("data", this.datosGenerales);
  }

  terminarIntento() {
    clearInterval(this.intervalo);
    this.tiempoHecho -= 3;
    let cantProblemas = this.puntuacion + (3 - this.vidas);
    if (cantProblemas != 0) {
      this.promXproblema = parseFloat(
        (this.tiempoHecho / cantProblemas).toFixed(2)
      );

      this.datosGenerales.tiempoJugado += this.tiempoHecho;
    } else {
      this.promXproblema = 0;
    }

    if (this.puntuacion > this.datosJuego.record) {
      //Guardar nuevo record
      this.audioFinBueno.play();
      this.nuevoRecord = true;
      this.datosJuego.record = this.puntuacion;
    } else {
      this.audioFinMalo.play();
      this.nuevoRecord = false;
    }

    this.datosJuego.intentos += 1;
    this.datosGenerales.intentos += 1;
    this.datosJuego.puntosAcum += this.puntuacion;
    this.guardarDatos();
    this.guardarDatosGenerales();
    this.finIntento = true;
  }

  //Controlador de tipo de pregunta
  crearPregunta() {
    let max = this.rangos[this.nivel][this.dificultad].max;
    let min = this.rangos[this.nivel][this.dificultad].min;

    let aux: number;
    let oprStr = "";
    let resultado: number;
    let modo = this.modo;

    if (this.modo == 5) {
      modo = this.getRandomInt(1, 4);
    }

    if (this.modo == 6) {
      modo = this.getRandomInt(1, 2);
    }

    if (this.modo == 7) {
      modo = this.getRandomInt(3, 4);
    }

    let numero1 = this.getRandomInt(min, max);

    // Evita la division entre 0
    if (modo == 4 && min == 0) {
      min += 1;
    }
    let numero2 = 0;

    // Crea multiplicaciones y divisiones faciles en los niveles más bajos ( < 3)
    if ((modo == 4 || modo == 3) && this.nivel < 3) {
      if (numero1 > max / 2) {
        numero2 = this.getRandomInt(min, max / 2);
      } else {
        numero2 = this.getRandomInt(max / 2, max);
      }
    } else {
      numero2 = this.getRandomInt(min, max);
    }
    // Crear divisiones o multiplicaciones con números negativos solo para el nivel difícil
    if (modo == 4 || (modo == 3 && this.dificultad == 2)) {
      let prob1 = Math.random();
      let prob2 = Math.random();

      if (prob1 > 0.75) numero1 = numero1 * -1;
      if (prob2 > 0.75) numero2 = numero2 * -1;
    }

    // Si es nivel principiante (quitar los resultados en restas negativos)
    let auxRes = 0;
    if (this.dificultad == 0) {
      if (modo == 2) {
        if (numero2 > numero1) {
          [numero2, numero1] = [numero1, numero2];
        }
      }
    }

    switch (modo) {
      case 1:
        resultado = numero1 + numero2;
        oprStr = "+";
        break;

      case 2:
        resultado = numero1 - numero2;
        oprStr = "-";
        break;
      case 3:
        resultado = numero1 * numero2;
        oprStr = "x";
        break;

      case 4:
        aux = numero1 * numero2;
        resultado = numero1;
        numero1 = aux;
        oprStr = "/";
        break;
      default:
        break;
    }

    this.pregunta = `${numero1}${oprStr}${numero2}`;

    // Verifica que no se repitan las preguntas
    if (this.historialPreguntas.includes(this.pregunta)) {
      return this.crearPregunta();
    } else {
      if (this.dificultad != 0) {
        this.historialPreguntas.push(this.pregunta);
      } else {
        if (this.ansPregunta === this.pregunta) {
          return this.crearPregunta();
        } else {
          this.ansPregunta = this.pregunta;
        }
      }
      this.correcta = resultado + "";
      this.respuestas = [];
      // Generar respuestas aleatorias 'inteligentes'
      this.respuestas = this.generarRespuestas(resultado, 2);
      // Mezclar el orden de las respuestas
      this.respuestas = this.shuffle(this.respuestas);
    }
  }

  responder(respuesta: string) {
    if (this.correcta === respuesta) {
      this.puntuacion += 1;
      this.rachaActual += 1;
      this.datosGenerales.correctos += 1;
      if (this.rachaMax < this.rachaActual) {
        this.rachaMax = this.rachaActual;
        this.datosJuego.rachaMax = this.rachaMax;
      }
      this.audioCorrect.currentTime = 0;
      this.audioCorrect.play();
    } else {
      this.datosGenerales.fallos += 1;
      this.vibration.vibrate(400);
      this.vidas -= 1;
      this.rachaActual = 0;
      this.audioIncorrect.currentTime = 0;
      this.audioIncorrect.play();
    }

    this.guardarDatosGenerales();

    this.pregsXnvl_param += 1;
    if (this.pregsXnvl_param == this.pregsXnvl) {
      this.pregsXnvl_param = 0;
      this.nivel += 1;
      this.pregsXnvl = this.rangos[this.nivel][this.dificultad].cantidad;
    }

    if (this.vidas == 0) {
      this.terminarIntento();
    } else {
      this.crearPregunta();
    }
  }

  iniciar() {
    this.reiniciarParametros();
    this.intervalo = setInterval(() => {
      this.controlTiempo();
    }, 1000);
    this.conteo = true;
    this.audioConteo.currentTime = 0;
    this.audioConteo.play();
  }

  reintentar() {
    this.reiniciarParametros();
    this.intervalo = setInterval(() => {
      this.controlTiempo();
    }, 1000);
    this.conteo = true;
    this.audioConteo.currentTime = 0;
    this.audioConteo.play();
  }

  reiniciarParametros() {
    this.segundos = this.tiempoInicial;
    this.tiempoHecho = 0;
    this.ultimosSegundos = false;
    this.tiempoString = this.parseTiempo(this.segundos);
    this.historialPreguntas = [];
    this.historial = [];
    this.nivel = 0;
    this.vidas = 3;
    this.puntuacion = 0;
    this.rachaActual = 0;

    this.crearPregunta();
  }

  // Creador de respuestas
  generarRespuestas(base: string | number, cantidad: number) {
    //Generar 'cantidad' de respuestas
    let respuestas = [];
    while (respuestas.length < cantidad) {
      //Tipos de respuesta Random
      let tipo = parseInt(Math.random() * 101 + "");
      let propuesta = base;
      let baseStr = base + "";
      let n = baseStr.length;

      //Suffle
      if (tipo >= 0 && tipo < 25 && baseStr.length > 1) {
        propuesta = parseInt(
          baseStr
            .split("")
            .sort(function() {
              return 0.5 - Math.random();
            })
            .join("")
        );
      }

      //Acercamiento a la respuesta en un intevalo bajo
      if (tipo >= 25 && tipo <= 60) {
        let porcentaje = parseInt(parseInt(base + "") / 10 + "");
        propuesta =
          parseInt(base + "") + this.randomInRange(-porcentaje, porcentaje);
      }

      //Diferencias de 10*size
      if (tipo > 60 && tipo <= 100) {
        let numero = this.randomInRange(-n, n);
        propuesta = parseInt(base + "") + numero * 10;
      }

      if (propuesta != base && !respuestas.includes(propuesta + ""))
        respuestas.push(propuesta + "");
    }
    respuestas.push(base + "");
    return respuestas;
  }

  // Funciones auxiliares
  parseTiempo(segundos: number) {
    let minString: string = segundos / 60 + "";
    let min = parseInt(minString);
    let seg = segundos % 60;

    let resultado = "";

    if (seg < 10) {
      resultado = `${min}:0${seg}`;
    } else {
      resultado = `${min}:${seg}`;
    }

    return resultado;
  }

  shuffle(a: any[] | string[]) {
    for (let i = a.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      [a[i], a[j]] = [a[j], a[i]];
    }
    return a;
  }

  randomInRange(min: string | number, max: string | number) {
    min = parseInt(min + "");
    max = parseInt(max + "");
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }

  toArray(cantidad: number) {
    var arreglo = [];
    for (let i = 0; i < cantidad; i++) {
      arreglo.push("");
    }
    return arreglo;
  }

  getRandomInt(min: number, max: number) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }

  ionViewWillLeave() {
    if (this.jugando && !this.finIntento) {
      this.terminarIntento();
    }
  }
}
