import { Component, OnInit } from "@angular/core";
import { Vibration } from "@ionic-native/vibration/ngx";
import { ApiService } from "../../services/api.service";
import { Storage } from "@ionic/storage";

@Component({
  selector: "app-trueorfalse",
  templateUrl: "./trueorfalse.page.html",
  styleUrls: ["./trueorfalse.page.scss"]
})
export class TrueorfalsePage implements OnInit {
  constructor(
    private storage: Storage,
    public vibration: Vibration,
    public api: ApiService
  ) {}
  color1: any = {
    r: 0,
    g: 0,
    b: 0
  };
  color2: any = {
    r: 0,
    g: 0,
    b: 0
  };
  puntos: number = 0;
  vidas: number = 3;
  record: number = 0;

  respuesta: boolean = false;
  isNewRecord: boolean = false;
  isJugando: boolean = true;
  nivel: number = 0;
  isMostrandoRes: boolean = false;
  isCorrec: boolean = false;

  usuarioStorage: any = {
    username: "",
    record: null
  };

  usuarioDB: any = {};
  datosJuego: any = {
    recordTF: 0
  };

  cargando: boolean = true;

  audioCorrect = new Audio("assets/audio/correcto.mp3");
  audioIncorrect = new Audio("assets/audio/incorrecto.mp3");
  audioFinNormal = new Audio("assets/audio/resultado_normal.mp3");
  audioFinBueno = new Audio("assets/audio/resultado_bueno.mp3");
  ngOnInit() {
    this.cargando = true;
    // Datos del usuario
    this.storage.get("usuario").then(val => {
      if (val.username) {
        this.usuarioStorage.username = val.username;
        this.getDatosUsuario();
      } else {
        console.log("Error al obtener usuario Storage");
      }
    });
    this.jugar();
  }

  jugar() {
    this.reiniciar();
    this.isJugando = true;
    this.generarColores();
  }

  generarAproximados(errorRelativo, exac) {
    let aprox;
    //TODO: Revisar formular para calcular los aproximados

    let mas = Math.random() > 0.5;
    // aprox = (errorRelativo * 255) / 100;

    if (mas) {
      //   aprox = aprox + exac;
      aprox = (errorRelativo * exac) / 100 + exac;
    } else {
      //   aprox = aprox - exac;
      aprox = (errorRelativo * exac) / 100 - exac;
    }

    if (aprox > 255) aprox = exac - aprox;
    aprox = Math.abs(aprox);

    return aprox;
  }

  generarColores() {
    this.color1 = this.randomColor();
    let aprox = Math.random() > 0.5 ? 0 : this.getAprox();

    //SOn iguales si el aprox es igual a 0;
    this.respuesta = aprox == 0;
    console.log("respuesta", this.respuesta);
    console.log(aprox);
    this.color2 = {
      r: Math.floor(this.generarAproximados(aprox, this.color1.r)),
      g: Math.floor(this.generarAproximados(aprox, this.color1.g)),
      b: Math.floor(this.generarAproximados(aprox, this.color1.b))
    };
  }

  randomColor() {
    let c = {
      r: 0,
      g: 0,
      b: 0
    };

    c.r = Math.floor(Math.random() * 255);
    c.g = Math.floor(Math.random() * 255);
    c.b = Math.floor(Math.random() * 255);
    return c;
  }

  getRGB(color) {
    return `rgb(${color.r}, ${color.g}, ${color.b})`;
  }

  getAprox() {
    if (this.nivel < 5) return 15;
    if (this.nivel < 10) return 13;
    if (this.nivel < 15) return 11;
    if (this.nivel < 20) return 7;
    if (this.nivel < 25) return 5;
    if (this.nivel < 30) return 3;
    if (this.nivel < 40) return 3;
    return 1;
  }

  responder(respuesta: boolean) {
    this.nivel += 1;

    if (this.respuesta == respuesta) {
      this.puntos += 1;
      console.log("correcto");
      this.isCorrec = true;
      this.audioCorrect.currentTime = 0;
      this.audioCorrect.play();

      if (this.puntos > this.record) {
        this.record = this.puntos;
        this.isNewRecord = true;
      }
    } else {
      console.log("incorrecto");
      this.vibration.vibrate(400);
      this.isCorrec = false;
      this.audioCorrect.currentTime = 0;
      this.audioIncorrect.play();
      this.vidas -= 1;
    }
    this.isMostrandoRes = true;

    setTimeout(() => {
      if (this.vidas == 0) {
        this.terminarJuego();
      } else {
        this.isMostrandoRes = false;
        this.generarColores();
      }
    }, 1000);
  }

  terminarJuego() {
    this.isJugando = false;
    if (this.isNewRecord) {
      this.record = this.puntos;
      this.isNewRecord = true;
      this.datosJuego.recordTF = this.puntos;
      this.usuarioDB.recordTF = this.puntos;
      this.updateRecord();
      this.audioFinBueno.play();
    } else {
      this.audioFinNormal.play();
    }
  }

  reiniciar() {
    this.isJugando = true;
    this.vidas = 3;
    this.puntos = 0;
    this.nivel = 0;
    this.isNewRecord = false;
    this.isMostrandoRes = false;
  }

  toArray() {
    let resultado = [];
    for (let i = 0; i < this.vidas; i++) {
      resultado.push(0);
    }

    return resultado;
  }
  getDatosUsuario() {
    this.cargando = true;
    let users = this.api.getUsuario(this.usuarioStorage.username);
    users.forEach(user => {
      if (user.length > 0) {
        let e = user[0];
        this.usuarioDB = {
          id: e.payload.doc.id,
          username: e.payload.doc.data()["username"],
          recordTF: e.payload.doc.data()["recordTF"]
            ? e.payload.doc.data()["recordTF"]
            : 0
        };
        console.log(this.usuarioDB);
        this.datosJuego.recordTF = this.usuarioDB.recordTF;
        this.record = this.usuarioDB.recordTF;
        this.cargando = false;
        return;
      } else {
        console.log("error");
        this.cargando = false;
      }
    });
  }

  updateRecord() {
    console.log("Actualizando datos");
    console.log(this.usuarioDB);

    let usuario = Object.assign({}, this.usuarioDB);
    delete usuario.id;
    this.api.update_User(this.usuarioDB.id, usuario);
  }
}
