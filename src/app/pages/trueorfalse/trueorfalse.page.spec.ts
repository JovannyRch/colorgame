import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrueorfalsePage } from './trueorfalse.page';

describe('TrueorfalsePage', () => {
  let component: TrueorfalsePage;
  let fixture: ComponentFixture<TrueorfalsePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrueorfalsePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrueorfalsePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
