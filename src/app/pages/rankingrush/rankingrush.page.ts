import { Component, OnInit } from "@angular/core";

import { ApiService } from "../../services/api.service";

import { ActivatedRoute } from "@angular/router";

@Component({
  selector: "app-rankingrush",
  templateUrl: "./rankingrush.page.html",
  styleUrls: ["./rankingrush.page.scss"]
})
export class RankingrushPage implements OnInit {
  constructor(private api: ApiService, private route: ActivatedRoute) { }

  jugadores: any[] = [];
  cargando: boolean = true;
  //Pagina de la que viene
  pageBefore: string;

  tipoRanking: string;
  titulo: string;

  ngOnInit() {
    this.tipoRanking = this.route.snapshot.paramMap.get("tipo");
    console.log(this.tipoRanking);
    if (this.tipoRanking === "rush") {
      this.ranking = "recordR";
      this.titulo = "Rush";
    }

    if (this.tipoRanking === "clasico") {
      this.ranking = "recordC";
      this.titulo = "Clásico";

    }

    if (this.tipoRanking === "tf") {
      this.titulo = "¿Son iguales?";
      this.ranking = "recordTF";
      console.log("TF :)");

    }

    this.getRanking();
  }

  ranking: string = "";

  getRanking() {


    this.cargando = true;
    this.api.getRankinkgRush(this.ranking, "desc").subscribe(data => {
      console.log(data);
      let index = 0;
      this.jugadores = data.map(e => {
        index++;
        return {
          id: e.payload.doc.id,
          username: e.payload.doc.data()["username"],
          record: e.payload.doc.data()[this.ranking],
          index
        };
      });
      this.cargando = false;
      console.log("jugadores", this.jugadores);
    });
  }

  restoJugadores() {
    if (this.jugadores.length > 3) {
      let resultado = [];
      for (let i = 3; i < this.jugadores.length; i++) {
        const j = this.jugadores[i];
        resultado.push(j);
      }
      return resultado;
    }
  }
}
