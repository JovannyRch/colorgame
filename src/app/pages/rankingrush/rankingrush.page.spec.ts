import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RankingrushPage } from './rankingrush.page';

describe('RankingrushPage', () => {
  let component: RankingrushPage;
  let fixture: ComponentFixture<RankingrushPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RankingrushPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RankingrushPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
