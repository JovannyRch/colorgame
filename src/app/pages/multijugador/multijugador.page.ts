import { Component, OnInit } from "@angular/core";
import { Storage } from "@ionic/storage";

import { ApiService } from "../../services/api.service";
import { Router } from "@angular/router";

@Component({
  selector: "app-multijugador",
  templateUrl: "./multijugador.page.html",
  styleUrls: ["./multijugador.page.scss"]
})
export class MultijugadorPage implements OnInit {
  usuarioStorage: any = {
    username: "",
    record: null
  };

  salaActual: any = {
    id: ""
  };

  usuarioDB: any = {};

  salas: any = [];
  uniendose: boolean = false;

  constructor(
    private api: ApiService,
    private storage: Storage,
    private router: Router
  ) {}

  salaNueva: any = {
    nombre: "",
    modo: "1",
    tiempo: "1",
    cantidadPreguntas: "10"
  };

  page: number = 1;
  ngOnInit() {
    // Configuración del juego
    this.storage.get("usuario").then(val => {
      if (val) {
        this.usuarioStorage = val;
        this.salaNueva.nombre = "Sala de " + this.usuarioStorage.username;
      }
      console.log(val);
    });

    this.getSalasDisponibles();
  }

  crearSala() {
    let jugadores = [];
    jugadores.push(this.usuarioStorage.username);
    let sala = {
      nombre: this.salaNueva.nombre,
      creador: this.usuarioStorage.username,
      jugadores,
      terminado: false,
      iniciado: false,
      cantidad: parseInt(this.salaNueva.cantidadPreguntas),
      puzzles: [],
      puntuaciones: [],
      reiniciar: false
    };

    console.log(sala);

    this.api
      .create("salas", sala)
      .then(response => {
        this.salaActual.id = response.id;
        this.router.navigate(["/sala/" + response.id]);
        console.log("Sala creada");
      })
      .catch(e => {
        console.log("Error al crear la sala");
      });
  }

  unirseSala(id) {
    this.uniendose = true;
    console.log("unirse a la sala", id);

    this.router.navigate(["/sala/" + id]);
    //this.getSala(id);
  }

  getSalasDisponibles() {
    this.api.getSalas().subscribe(data => {
      this.salas = data.map(e => {
        let data = e.payload.doc.data();
        return {
          id: e.payload.doc.id,
          jugadores: data["jugadores"],
          terminado: data["terminado"],
          iniciado: data["iniciado"],
          creador: data["creador"],
          nombre: data["nombre"]
        };
      });
      console.log(this.salas);
    });
  }

  updateSala() {
    let sala = Object.assign({}, this.salaActual);
    delete sala.id;
    this.api.update("salas", this.salaActual.id, sala);
  }

  getSala(creador) {
    console.log("Leyendo sala actual");
    console.log(creador);

    return this.api.getSala(creador).subscribe(data => {
      if (data.length > 0) {
        let e = data[0];
        let d = e.payload.doc.data();
        this.salaActual = {
          id: e.payload.doc.id,
          jugadores: d["jugadores"],
          terminado: d["terminado"],
          iniciado: d["terminado"],
          creador: d["creador"]
        };
        console.log("sala actual");
        console.log(this.salaActual);
        this.router.navigate(["/sala/" + this.salaActual.id]);
        return;
        //SI no soy el creador, me uno a la sala y actualizo

        // if (this.salaActual.creador === this.usuarioStorage.username) {
        //   console.log("Soy el creador");
        // } else {
        //   console.log(this.salaActual.jugadores);

        //   //Si no estoy ya en la sala, me uno
        //   if (
        //     !this.salaActual.jugadores.includes(this.usuarioStorage.username)
        //   ) {
        //     console.log("me uno a la partida");
        //     this.salaActual.jugadores.push(this.usuarioStorage.username);
        //     this.updateSala();
        //   } else {
        //     //ESTas jugando
        //     console.log("Estoy jugando");
        //   }
        // }
      } else {
        //Sala no encontrada
        console.log("Sala no encontrada");
      }
      return;
    });
  }
}
