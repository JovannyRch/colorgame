import { Component, OnInit, OnDestroy } from "@angular/core";
import { Storage } from "@ionic/storage";

@Component({
  selector: "app-about",
  templateUrl: "./about.page.html",
  styleUrls: ["./about.page.scss"]
})
export class AboutPage implements OnInit, OnDestroy {
  constructor(private storage: Storage) {}

  conf: any = {
    dificultad: 1,
    tiempo: 1,
    temaOscuro: false,
    idioma: null
  };

  mensajes: any = {
    titulo: { 1: "Acerca de", 2: "About" },
    info: {
      1: "Es un juego que pone a prueba tus habilidades de cálculo mental, supera tu propio record o juega contra un amigo.",
      2: "Its a game that challenges your mental calculation skills, beat your own record or play against a friend."
    },
    version: { 1: "Versión", 2: "Version" },
    tecnologia: { 1: "Tecnología ocupada", 2: "Build it with" },
    desarrollador: { 1: "Sobre el desarrollador", 2: "About developer" },
    bio: {
      1: "Mi nombre es Jovanny Rch (j19), soy de México y me gusta programar y jugar fútbol",
      2: "My name is Jovanny Rch (j19), I from Mexico and I love to program and to play soccer"
    },
    contacto: { 1: "Mis datos de contacto", 2: "Contact me" }
  };

  icon: string = "rocket";

  intervalo: any;
  ngOnDestroy() {
    clearInterval(this.intervalo);
  }

  ngOnInit() {
    // Configuración del juego
    this.storage.get("conf").then(val => {
      if (val) this.conf = val;
      else this.storage.set("conf", this.conf);
    });

    this.intervalo = setInterval(() => {
      if (this.icon == "rocket") this.icon = "planet";
      else this.icon = "rocket";
    }, 2000);
  }
}
