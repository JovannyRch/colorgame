import { AngularFirestore } from "@angular/fire/firestore";
import { Injectable } from "@angular/core";

@Injectable({
  providedIn: "root"
})
export class ApiService {
  constructor(private db: AngularFirestore) { }

  create_NewUser(record) {
    return this.db.collection("usuarios").add(record);
  }

  read_Users() {
    return this.db.collection("usuarios").snapshotChanges();
  }

  update_User(recordID, record) {
    console.log("Actualizando record");
    console.log(recordID);
    console.log(record);

    this.db
      .doc("usuarios/" + recordID)
      .update(record)
      .then(ok => {
        console.log("Usuario actualizado", ok);
      })
      .catch(e => {
        console.log("ocurrio un error", e);
      });
  }

  delete_User(record_id) {
    this.db.doc("usuarios/" + record_id).delete();
  }

  getUsuario(username: string) {
    var allUsers = this.db.collection("usuarios", ref =>
      ref.where("username", "==", username).limit(1)
    );
    var users = allUsers.snapshotChanges();
    return users;
  }

  getRankinkgRush(record: string, order: string) {
    var allUsers = this.db.collection("usuarios", ref =>
      ref.orderBy(record, order === "asc" ? "asc" : "desc").where(record, ">", 0)
    );
    var users = allUsers.snapshotChanges();
    return users;
  }

  create(table, record) {
    return this.db.collection(table).add(record);
  }

  get(table) {
    // return this.db
    //   .collection(table, ref => ref.where(ref.id, "==", id))
    //   .snapshotChanges();

    return this.db.collection(table).snapshotChanges();
  }

  getSala(creador) {
    return this.db
      .collection("salas", ref => ref.where("creador", "==", creador))
      .snapshotChanges();
  }

  read(table) {
    return this.db.collection(table).snapshotChanges();
  }

  update(table, recordID, record) {
    this.db.doc(table + "/" + recordID).update(record);
  }

  delete(table, record_id) {
    this.db.doc(table + "/" + record_id).delete();
  }

  getSalas() {
    return this.db
      .collection("salas", ref =>
        ref.where("terminado", "==", false).where("iniciado", "==", false)
      )
      .snapshotChanges();
  }
}
