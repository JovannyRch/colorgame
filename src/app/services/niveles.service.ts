import { Injectable } from "@angular/core";

@Injectable({
  providedIn: "root"
})
export class NivelesService {
  constructor() {}

  niveles: {
    0: [{ min: 56; max: 60 }, { min: 30; max: 40 }, { min: 40; max: 50 }];
    1: [{ min: 50; max: 55 }, { min: 20; max: 30 }, { min: 30; max: 40 }];
    2: [{ min: 45; max: 50 }, { min: 10; max: 20 }, { min: 20; max: 30 }];
    3: [{ min: 35; max: 43 }, { min: 30; max: 40 }, { min: 40; max: 50 }];
    4: [{ min: 56; max: 60 }, { min: 30; max: 40 }, { min: 40; max: 50 }];
    5: [{ min: 56; max: 60 }, { min: 30; max: 40 }, { min: 40; max: 50 }];
    6: [{ min: 56; max: 60 }, { min: 30; max: 40 }, { min: 40; max: 50 }];
    7: [{ min: 56; max: 60 }, { min: 30; max: 40 }, { min: 40; max: 50 }];
    8: [{ min: 56; max: 60 }, { min: 30; max: 40 }, { min: 40; max: 50 }];
    9: [{ min: 56; max: 60 }, { min: 30; max: 40 }, { min: 40; max: 50 }];
    10: [{ min: 56; max: 60 }, { min: 30; max: 40 }, { min: 40; max: 50 }];
    11: [{ min: 56; max: 60 }, { min: 30; max: 40 }, { min: 40; max: 50 }];
    12: [{ min: 56; max: 60 }, { min: 30; max: 40 }, { min: 40; max: 50 }];
    13: [{ min: 56; max: 60 }, { min: 30; max: 40 }, { min: 40; max: 50 }];
    14: [{ min: 56; max: 60 }, { min: 30; max: 40 }, { min: 40; max: 50 }];
    15: [{ min: 56; max: 60 }, { min: 30; max: 40 }, { min: 40; max: 50 }];
  };

  rush(nivel: number) {
    return this.niveles[nivel];
  }

  getRangos(nivel){
    return null;
  }
}
