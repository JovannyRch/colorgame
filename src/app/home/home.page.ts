import { Component, OnInit, OnDestroy } from "@angular/core";
import { Storage } from "@ionic/storage";

import { Router } from "@angular/router";

@Component({
  selector: "app-home",
  templateUrl: "home.page.html",
  styleUrls: ["home.page.scss"]
})
export class HomePage implements OnInit {
  usuario: any = {
    dificultad: 1,
    tiempo: 1,
    temaOscuro: "",
    dosJugadores: false,
    idioma: "",
    username: ""
  };

  icon: string = "";

  intervalo: any;
  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    clearInterval(this.intervalo);
  }

  bgColor: any = {
    r: 0,
    g: 0,
    b: 0,
  }

  constructor(private storage: Storage, private router: Router) { }
  ngOnInit(): void {
    let opcIcon = parseInt(Math.random() * 2 + "");

    if (opcIcon == 0) this.icon = "rocket";
    if (opcIcon == 1) this.icon = "planet";

    // Configuración del juego
    this.storage.get("usuario").then(val => {
      if (val) this.usuario = val;
    });
    this.bgColor = this.randomColor();

    this.intervalo = setInterval(() => {
      this.reloj();
    }, 1500);
  }

  iniciar() {
    if (!this.usuario.username) {
      this.router.navigate(["/registro"]);
    } else {
      this.router.navigate(["/menu1"]);
    }

  }

  getRGB(color, i) {
    return `rgb(${color.r}, ${color.g}, ${color.b},${i})`;
  }

  reloj() {
    this.bgColor = this.randomColor();
  }

  randomColor() {
    let r = {
      r: 0,
      g: 0,
      b: 0
    };
    r.r = Math.floor(Math.random() * 255);
    r.g = Math.floor(Math.random() * 255);
    r.b = Math.floor(Math.random() * 255);
    return r;
  }

}
