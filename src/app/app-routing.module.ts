import { NgModule } from "@angular/core";
import { PreloadAllModules, RouterModule, Routes } from "@angular/router";

const routes: Routes = [
  { path: "", redirectTo: "home", pathMatch: "full" },
  {
    path: "home",
    loadChildren: () => import("./home/home.module").then(m => m.HomePageModule)
  },
  { path: "menu1", loadChildren: "./pages/menu1/menu1.module#Menu1PageModule" },
  {
    path: "juego/:modo/:dificultad/:tiempo",
    loadChildren: "./pages/juego/juego.module#JuegoPageModule"
  },
  {
    path: "estadisticas",
    loadChildren:
      "./pages/estadisticas/estadisticas.module#EstadisticasPageModule"
  },
  { path: "about", loadChildren: "./pages/about/about.module#AboutPageModule" },
  {
    path: "dosjugadores/:modo/:dificultad/:tiempo",
    loadChildren:
      "./pages/dosjugadores/dosjugadores.module#DosjugadoresPageModule"
  },
  {
    path: "language",
    loadChildren: "./pages/language/language.module#LanguagePageModule"
  },
  {
    path: "registro",
    loadChildren: "./pages/registro/registro.module#RegistroPageModule"
  },
  { path: "rush", loadChildren: "./pages/rush/rush.module#RushPageModule" },
  {
    path: "multijugador",
    loadChildren:
      "./pages/multijugador/multijugador.module#MultijugadorPageModule"
  },
  {
    path: "rankingrush/:tipo",
    loadChildren: "./pages/rankingrush/rankingrush.module#RankingrushPageModule"
  },
  {
    path: "clasico",
    loadChildren: "./pages/clasico/clasico.module#ClasicoPageModule"
  },
  {
    path: "ranking-clasico",
    loadChildren:
      "./pages/ranking-clasico/ranking-clasico.module#RankingClasicoPageModule"
  },
  {
    path: "nuevemillones",
    loadChildren:
      "./pages/nuevemillones/nuevemillones.module#NuevemillonesPageModule"
  },
  {
    path: "sala/:sala",
    loadChildren: "./pages/sala/sala.module#SalaPageModule"
  },
  { path: 'randomcolor', loadChildren: './pages/randomcolor/randomcolor.module#RandomcolorPageModule' },
  { path: 'trueorfalse', loadChildren: './pages/trueorfalse/trueorfalse.module#TrueorfalsePageModule' }

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
